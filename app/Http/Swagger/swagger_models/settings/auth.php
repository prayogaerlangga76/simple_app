<?php

/**
* @OA\SecurityScheme(
*      securityScheme="bearerAuth",
*      in="header",
*      name="bearerAuth",
*      type="https",
*      scheme="bearer",
*      bearerFormat="JWT",
* )
*/
