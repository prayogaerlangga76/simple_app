<?php

/**
 * @OA\Info(
 *     description="Contoh API doc menggunakan OpenAPI/Swagger",
 *     version="1.0.0",
 *     title="Contoh API documentation",
 *     termsOfService="https://swagger.io/terms/",
 *     @OA\Contact(
 *         email="prayogaerlangga75@gmail.com"
 *     ),
 *     @OA\License(
 *         name="Apache 2.0",
 *         url="https://www.apache.org/licenses/LICENSE-2.0.html"
 *     )
 * )
 */
