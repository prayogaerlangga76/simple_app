<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class ValidationController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    /**
* @OA\SecurityScheme(
*      securityScheme="bearerAuth",
*      in="header",
*      name="bearerAuth",
*      type="http",
*      scheme="bearer",
*      bearerFormat="JWT",

* )
*/
    /**
     * @OA\Get(
     *     path="/api/validation",
     *     tags={"simple_app"},
     *     summary="Returns a Sample API response",
     *     description="A sample greeting to test out the API",
     *     operationId="validation",
     *     security={{"bearer_token":{}}},
     *     @OA\Response(
     *         response="default",
     *         description="successful operation"
     *     )
     * )
     */
    
    public function __invoke(Request $request)
    {
        $user = JWTAuth::parseToken()->getPayload();

            return response()->json([
                'is_valid' => true,
                'expired_at'=> date('d-m-Y', $user->get('exp')),
                'username' => auth()->user()->username,
            ]); 
        }
        // eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vc2ltcGxlX2FwcC50ZXN0L2FwaS9sb2dpbiIsImlhdCI6MTY1NjA3ODQ0NywiZXhwIjoxNjU2Njc4Mzg3LCJuYmYiOjE2NTYwNzg0NDcsImp0aSI6IlBwbUV6blJLVjhpU3l2TFYiLCJzdWIiOiJhMjE1MDlhMy04ZDBhLTQzZGItYWQwYi1mNjAyZjA3ZDkxZDMiLCJwcnYiOiIyM2JkNWM4OTQ5ZjYwMGFkYjM5ZTcwMWM0MDA4NzJkYjdhNTk3NmY3In0.pDmyHMUnQe2T_P7XzptIY2wY0fenTQfDWlRgu018lyo
}
