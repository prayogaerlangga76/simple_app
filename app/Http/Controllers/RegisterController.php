<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    /**
     * @OA\Post(
     *     path="/api/register",
     *     tags={"simple_app"},
     *     summary="Returns a Sample API response",
     *     description="A sample greeting to test out the API",
     *     operationId="greet",
     *     @OA\Parameter(
     *          name="username",
     *          description="Isi dengan username",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Parameter(
     *          name="role",
     *          description="isi dengan role",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *     ),
     *     @OA\Response(
     *         response="default",
     *         description="successful operation"
     *     )
     * )
     */
    public function __invoke(Request $request)
    {
        $allRequest = $request->all();
        $validator = Validator::make($allRequest, [
            'username' => 'required|unique:users',
            'role' => 'required' 
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $password = Str::random(6);
        try{
            $user = User::create([
                'username' => $allRequest['username'],
                'password' => bcrypt($password),
                'role' => $allRequest['role']
            ]); 

            return response()->json([
                'success' => true,
                'message' => 'Data user berhasil ditambahkan!',
                'data' => [
                   'user' => $allRequest['username'],
                   'password' => $password,
                   'role' => $allRequest['role'],
                   'id' => $user->id
                    ]
            ], Response::HTTP_OK);
        }catch(\Exception $e){
            dd($e->getMessage());
        }
    }
}
