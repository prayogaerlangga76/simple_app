# Instalasi
```
1. git clone https://gitlab.com/prayogaerlangga76/simple_app.git
2. cd panel
3. composer install
4. cp .env.example .env
5. Database Configuration
    - open file .env
    - change : 
        DB_CONNECTION=mysql
        DB_HOST=127.0.0.1
        DB_PORT=3306
        DB_DATABASE=panel_db (menyesuaikan dengan database yang dibuat)
        DB_USERNAME=root
        DB_PASSWORD=
    - save file
6. php artisan key:generate
7. php artisan migrate
8. php artisan l5-swagger:generate
9. php artisan serve
10. akses documentasi api http://127.0.0.1:8000/api/documentation




```
(optional) <br>
Untuk dokumentasi api validation di swagger belum bisa , harap coba melalui post-man berikut dokumentasinya
https://documenter.getpostman.com/view/19689090/UzBqq5hH
# Deploy
link heroku : http://simple-app-jds.herokuapp.com/api/documentation#/

# Docker Image
Belum selesai